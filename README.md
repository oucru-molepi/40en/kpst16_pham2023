# *Klebsiella pneumoniae* ST16 in Hanoi, Vietnam during 2017-2018

This repos is dedicated for analyses on the *Klebsiella pneumoniae* ST16 (KpST16) sequences from the study of [Pham *et al*, 2023](https://doi.org/10.1016/S2666-5247(22)00338-X).

These were isolates from adults admitted to ICUs at the National Hospital for Tropical Diseases and Bach Mai Hospital in Hanoi, Vietnam, between June 19, 2017, and Jan 16, 2018.

Number of isolates: N=67.

This is a subset of a larger study of [Roberts *et al*, 2022](https://www.thelancet.com/journals/lanmic/article/PIIS2666-5247(22)00181-1/fulltext), with study accession numbers: **PRJEB28400** and **PRJEB29424**.

## Data
Raw files are a subset in the directory `raw_dir="/data/SiNguyen/40EN/raw_read/Kp_ST16_PRJEB28400_PRJEB29424"`.  
The list of run accession numbers for the isolates is: `list_file="KpST16_Pham2023.txt"`

## Core pseudo-genomes of 67 Hanoi isolates, with Snippy (24/07/23)
```bash
mkdir -p snippy
cd snippy

### Snippy input
raw_dir="/data/SiNguyen/40EN/raw_read/Kp_ST16_PRJEB28400_PRJEB29424"
list_file="KpST16_Pham2023.txt"

while read -r sampleID; do
    forward="${raw_dir}/${sampleID}_1.fastq.gz";
    reverse="${raw_dir}/${sampleID}_2.fastq.gz";
    echo -e $sampleID"\t"$forward"\t"$reverse >> samples.tsv;
done < $list_file

refchrom="/data/SiNguyen/40EN/references/UCLAOXA232KP_GCF_001741665.1/GCF_001741665.1_chrom.fasta"

### Run Snippy
conda activate base
snippy-multi samples.tsv --ref $refchrom --cpus 4 \
    --mapqual 60 --basequal 13 --mincov 5 --minfrac 0.9 > run.sh
sh ./run.sh

snippy-clean_full_aln core.full.aln > clean.full.aln
```

## Combine core pseudo-genomes of 67 Hanoi isolates with 69 HTD Ho Chi Minh isolates (24/07/23)

```bash
mkdir -p plusHTD_snippy_gubbins
conda activate base
faSomeRecords.py --exclude -f ./snippy/clean.full.aln -r Reference -o ./plusHTD_snippy_gubbins/clean.full.aln

htd_file="/data/SiNguyen/40EN/first99/KpST16_69VNstrains/snippy_gubbins/clean.full.aln"
cat $htd_file >> ./plusHTD_snippy_gubbins/clean.full.aln

# Check. Should returns 137 sequences, all with 5312206 bp
fainfo_hc4 ./plusHTD_snippy_gubbins/clean.full.aln
```

## Refine core pseudo-genomes of 136 sequences from Hanoi & HTD Ho Chi Minh (24/07/23)

```bash
cd plusHTD_snippy_gubbins
mkdir -p gubbins
cd gubbins

aln="clean.full.aln"
mv "../"$aln .

conda activate gubbins
run_gubbins.py \
    --threads 10 \
    -v -t hybrid --iterations 20 \
    $aln > gubbins_run.log 2>&1

cd ..
mv gubbins/$aln .
```

```bash
awk '{OFS="\t"} {if (NF==3) {print $1,"recombination",$3}}' < \
    ./gubbins/clean.full.recombination_predictions.embl > \
    ./recombination_gubbins.plus_prophage.embl

phage_file="/data/SiNguyen/40EN/references/UCLAOXA232KP_GCF_001741665.1/GCF_001741665.1_chrom.PHASTER/phage_regions.fna"

grep ">" $phage_file | awk '{print $2}' | \
    sed 's/-/../g' | awk '{OFS="\t"} {print "FT", "prophage", $1}' >>\
    ./recombination_gubbins.plus_prophage.embl

conda activate srst2
remove_blocks_from_aln.py -c \
    -a clean.full.aln \
    -t recombination_gubbins.plus_prophage.embl \
    -o KpST16_136VNstrains_refinedSnippy.fasta

# Found 278 regions
# Adjusted 137 sequences
# Original alignment length: 5312206      New alignment length:4972077
# Done.
```

## Exclude Reference sequence, then Call SNP sites (24/07/23)

```bash
cd plusHTD_snippy_gubbins

conda activate base
faSomeRecords.py --exclude -f KpST16_136VNstrains_refinedSnippy.fasta -r Reference -o KpST16_136VNstrains_refinedSnippy.noRef.fasta
# Check. Below command should return 136 seq, all with 4972077 bp
fainfo_hc4 KpST16_69VNstrains_refinedSnippy.noRef.fasta

snp-sites -o KpST16_136VNstrains_refinedSnippy.noRef.snp.fasta \
    KpST16_136VNstrains_refinedSnippy.noRef.fasta

fainfo_hc4 KpST16_136VNstrains_refinedSnippy.noRef.snp.fasta
```

> INPUT: n=1 (KpST16_136VNstrains_refinedSnippy.noRef.snp.fasta)  
        136 sequences  
        136 have 594 bp  

## Build phylogeny, from refined SNP alignment from Snippy, with IQ-TREE v2.0.3

```bash
conda activate gubbins
mkdir -p plusHTD_iqtreemodel
cd plusHTD_iqtreemodel
cp ../plusHTD_snippy_gubbins/KpST16_136VNstrains_refinedSnippy.noRef.snp.fasta .
iqtree -s KpST16_136VNstrains_refinedSnippy.noRef.snp.fasta -m TEST -T 8 
```
Best-fit model: K3P+ASC chosen according to BIC

```bash
mkdir -p plusHTD_iqtree
cd plusHTD_iqtree
cp ../plusHTD_snippy_gubbins/KpST16_136VNstrains_refinedSnippy.noRef.snp.fasta .
# number of bootstrap replicates = 1000
iqtree -s KpST16_136VNstrains_refinedSnippy.noRef.snp.fasta -m K3P+ASC -B 1000 -T 8 
```
Using FigTree, export tree file as Newick format, midpoint rooted.


## Build phylogeny with Reference sequence, and RAxML (25/07/23)

```bash
cd plusHTD_snippy_gubbins
snp-sites -o KpST16_136VNstrains_refinedSnippy.snp.fasta \
    KpST16_136VNstrains_refinedSnippy.fasta

fainfo_hc4 KpST16_136VNstrains_refinedSnippy.snp.fasta
# 137 sequences of 638 bp

cd ..
mkdir -p plusHTD_raxml
cd plusHTD_raxml

conda activate gubbins
raxmlHPC-PTHREADS-SSE3 -f a -m GTRGAMMA -p 101 -x 101 -N 100 -T 10 \
-s ../plusHTD_snippy_gubbins/KpST16_136VNstrains_refinedSnippy.snp.fasta \
-n KpST16_136VNstrains
```
